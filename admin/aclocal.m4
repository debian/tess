
AC_PREREQ(2.52)

AC_DEFUN(MN_UPPERCASE,	#  {{{	clone of JD_UPPERCASE from S-Lang
[
changequote(<<, >>)dnl
define(<<$2>>, translit($1, [a-z], [A-Z]))dnl
changequote([, ])dnl
])
#}}}

AC_DEFUN(MN_VERSION_STRING_TO_INT, dnl#{{{
[
   num_fields=`echo $1 | awk -F. '{print NF}'`
   if test $num_fields -ne 3 ; then
	AC_MSG_ERROR([$1 not in <major>.<minor>.<patch> version form])
   fi
   major_ver=`echo $1 | awk -F. '{print  [$]1 * 10000}'`
   minor_ver=`echo $1 | awk -F. '{print  [$]2 * 100}'`
   patchlev=`echo $1 | awk -F. '{print [$]3}'`
   let MN_VERSION_INT=$major_ver+$minor_ver+$patchlev
])}}}

AC_DEFUN(MN_PACKAGE_INIT, dnl#{{{
[

if ! test -s VERSION ; then
   AC_MSG_ERROR([could not find VERSION file])
fi

MN_UPPERCASE(AC_PACKAGE_NAME,MODNAME)

MODNAME[]_VERSION_STRING=`cat VERSION`
MODNAME[]_VERSION=MN_VERSION_STRING_TO_INT([$]MODNAME[]_VERSION_STRING)

if test -f REVISION ; then 
   REVISION=`cat REVISION | grep -v \#`
else
   REVISION=
fi
MODNAME[]_MODULE_REVISION=$REVISION

AC_SUBST(MODNAME[]_VERSION)
AC_SUBST(MODNAME[]_VERSION_STRING)
AC_SUBST(MODNAME[]_REVISION)
])
#}}}

AC_DEFUN(MN_WITH_PROG, dnl#{{{
[
   test "x$prefix" = "xNONE" && prefix="$ac_default_prefix"
   MN_UPPERCASE($1,MN_ARG1)
   AC_ARG_WITH($1,
   [  --with-$1=DIR        Find $1 executable within DIR],
   [mn_with_$1_arg=$withval], [mn_with_$1_arg=no])
   case "x$mn_with_$1_arg" in
	xno)
	;;
	x)
	AC_MSG_ERROR(--with-$1 requires a value)
	;;
	*)
	if test -x $mn_with_$1_arg/$1 ; then
	   MN_ARG1[]_DIR=$mn_with_$1_arg
	else
	   AC_MSG_ERROR($1 not found at $mn_with_$1_arg)
	fi
  ;;
esac
if test -z "$MN_ARG1[]_DIR" ; then
   echo "MN_ARG1 directory not specified, searching for it ..."
   AC_PATH_PROG(MN_ARG1[]_DIR,[$1],[],[$prefix/bin $PATH /usr/bin /usr/local/bin /opt/local/bin /sw/bin ])
   if test -z "$MN_ARG1[]_DIR" ; then
	AC_MSG_ERROR(Could not find required component: $1)
   else
	MN_ARG1[]_DIR=`AS_DIRNAME($MN_ARG1[]_DIR)`
   fi
fi
AC_SUBST(MN_ARG1[]_DIR)
])
#}}}

AC_DEFUN(MN_CHECK_SLSH, dnl#{{{
[
   MN_WITH_PROG(slsh)
   MN_VERSION_STRING_TO_INT($1)
   AC_MSG_NOTICE(SLang version as reported by slsh)

   SLANG_VERSION_STRING=`$SLSH_DIR/slsh <<EOT
() = printf("%S\n",_slang_version_string);
EOT`

   SLANG_VERSION=`$SLSH_DIR/slsh <<EOT
() = printf("%S\n",_slang_version);
EOT`

   if test $SLANG_VERSION -lt $MN_VERSION_INT ; then
	AC_MSG_ERROR(SLang >= $1 is required; you have $SLANG_VERSION_STRING)
   fi


   datadir_name=`basename $datadir`
   SL_FILES_INSTALL_DIR=$prefix/$datadir_name/slsh/local-packages

   AC_SUBST(SLANG_VERSION)
   AC_SUBST(SLANG_VERSION_STRING)
   AC_SUBST(SL_FILES_INSTALL_DIR)
])}}}
