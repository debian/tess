
require("tess");

variable f = &tess_auto_summarize;

tess_invoke(1, f, 12.5);
tess_invoke(1, f, "bad");
tess_invoke(1, f, NULL);
tess_invoke(0, f, 0);			% NB: no summary will be emitted

tess_invoke(1, &tess_catch_type_errors, "strings_are_not_legal_here");
tess_invoke(1, &tess_catch_type_errors, NULL);
tess_invoke(0, &tess_catch_type_errors, 0);
tess_invoke(0, &tess_catch_type_errors, 1);
