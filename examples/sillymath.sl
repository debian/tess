	define add()
	{
	   variable i, j;
	   if (_NARGS < 2) usage("add(i,j)");
	   (i,j) = ();
	   return i+j;
	}

	define subtract()
	{
	   variable i, j;
	   if (_NARGS < 2) usage("subtract(i,j)");
	   (i,j) = ();
	   return i - j;
	}
