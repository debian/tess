
require("tess");

variable f = &subtract;
tess_invoke(1, f);
tess_invoke(1, f, "hi there!");
tess_invoke(1, f, 2);
tess_invoke(0, f, 2, 3);
	
tess_invoke(1, f, "one", 2);
tess_invoke(1, f, "hello", " there!");
