
%!%+ tess {{{
%\function{tess}
%\description
% TESS is the (Te)st (S)ystem for (S)-Lang, which aims at reducing the
% workload and ad-hoc nature of regression testing S-Lang software by
% by collecting common testing elements into a single, easy-to-use
% framework.  TESS provides the S-Lang developer nominal mechanisms
% for tailoring the S-Lang environment and invoking functions with
% arbitrary inputs, while transparently inspecting and cleaning the
% stack, gathering pass/fail statistics, and providing error recovery
% from exceptions.
%
% Copyright (C) 2004-2007 Massachusetts Institute of Technology 
% Michael S. Noble <mnoble@space.mit.edu>
%\seealso{tess_invoke, tess_summary, tess_add_eval_paths}
%!%-  }}}

% Private TESS interface {{{

#ifeval _slang_version < 20000
   verror("SLang 2 or greater required.  You have %S", _slang_version_string);
#endif

private variable PassFail = [ "FAILED", "PASSED" ];

private variable Expectations = [
   "SHOULD NOT signal error",
   "SHOULD signal error"
];

private variable Indications = [ "DID NOT", "DID" ];
private variable Results = Integer_Type[2];
private variable TestCase  = 0;

% Some helpful (and harmless) assumptions about distribution directories
private variable Src = path_concat("..","src");
private variable Packages = path_concat("..","packages");
private variable Share = path_concat("..","share");


private define report(func, error_signaled, should_signal_error)
{
   variable passed = (error_signaled == should_signal_error);

   Results[passed]++;

   () = fprintf(stderr,"\nTest Case %d: %s: %s (%s, %s)\n\n",
		TestCase,
		sprintf("%S",func)[[1:]],
		PassFail[passed],
		Expectations[should_signal_error],
		Indications[error_signaled]);
   
   variable x = _stkdepth();
   if (x) {
	() = fprintf(stderr,"Stack Contents:\n");
	_print_stack();
	_pop_n(_stkdepth());
	() = fprintf(stderr,"\n");
   }

   () = fflush(stderr);
}
% }}}

% Versioning {{{
% Keep this in synch with VERSION and REVISION files
private variable _version_string = "0.3.0";
private variable _version = array_map(Integer_Type,
			&integer, strtok(_version_string, "."));
_version = 10000*_version[0] + 100*_version[1] + _version[2];
public define _tess_version_string () { return _version_string; }
public define _tess_version() { return _version; }
% }}}

public variable Component; % {{{
#if$TESS_COMPONENT
Component = getenv("TESS_COMPONENT");
#else
#  ifexists __argv
   Component = path_sans_extname(path_basename(__argv[__argc - 1]));
#  else
   Component = "unspecified";
#  endif
#endif % }}}

private variable _tess_auto_summarize = 1; % {{{
public define tess_auto_summarize()
%!%+{{{
%\function{tess_auto_summarize}
%\synopsis{Turn automatic test suite summarization on or off}
%\usage{tess_add_eval_paths( [0 | 1.])}
%\description
%\notes
%\seealso{tess_summary}
%!%- }}}
{
   if (_NARGS)
	_tess_auto_summarize = int(());
   else
	_tess_auto_summarize = 1;
} % }}}

public define tess_add_eval_paths() % {{{
%!%+{{{
%\function{tess_add_eval_paths}
%\synopsis{Add one or more directories to the S-Lang evalfile() search path}
%\usage{tess_add_eval_paths( path1, [path2, ...])}
%\description
% This function is a convenience wrapper around the set_slang_load_path()
% function, making it cleaner and simpler to augment the list of directories
% searched by the S-Lang interpreter when evalfile() is invoked with an
% ambiguous file specification.
%\notes
% TESS automatically appends the current working directory, as well as ../src,
% ../share, and ../packages to the load path.
%\seealso{tess_add_import_paths}
%!%- }}}
{
    foreach(__pop_args(_NARGS)) {
	variable path = ().value;
	set_slang_load_path( strcat(path, ":", get_slang_load_path()) );
    }
} % }}}

public define tess_add_import_paths() % {{{
%!%+{{{
%\function{tess_add_import_paths}
%\synopsis{Add one or more directories to the S-Lang import() search path}
%\usage{tess_add_import_paths( path1, [path2, ...])}
%\description
% This function is a convenience wrapper around the set_import_module_path()
% function, making it cleaner and simpler to augment the list of directories
% searched by the S-Lang interpreter when import() is invoked.
%\notes
% TESS automatically appends ../src to the import path.
%\seealso{tess_add_eval_paths}
%!%- }}}
{
    foreach(__pop_args(_NARGS)) {
	variable path = ().value;
	set_import_module_path( strcat(path, ":", get_import_module_path()) );
    }
} % }}}

public define tess_catch_type_errors() % {{{
%!%+{{{
%\function{tess_catch_type_errors}
%\synopsis{Give S-Lang ERROR block mechanism the ability to catch type mismatch errors}
%\usage{tess_catch_type_errors( [yes_or_no] ) }
%\description
% This function augments the S-Lang ERROR block mechanism, giving it the
% ability to catch type mismatch exceptions (which S-Lang 1.x formally
% considers uncatchably fatal).  This feature is useful for a test
% framework, since it allows functions to be safely exercised against
% a wide variety of types.
%
% If the first passed argument evaluates to a boolean TRUE then the function
% will enable type error catching.  If either zero arguments are passed,
% or the first argument evaluates to boolean FALSE, then type error catching
% will be disabled.
%\notes
% This function is deprecated, as in S-Lang 2 all exceptions may be caught.
%\seealso{tess_invoke}
%!%- }}}
{
   variable yes_or_no = 0;
   if (_NARGS > 0) {
	_pop_n(_NARGS - 1);
	yes_or_no = ();
   }
   % no-op in SLang 2
} % }}}

public define tess_invoke() % {{{
%!%+{{{
%\function{tess_invoke}
%\synopsis{Execute a test case}
%\usage{tess_invoke( expected_to_fail, function_ref [, arg1, arg2, ...]) }
%\description
% Invoke the given function (by dereference), optionally passing in one
% or more arguments.  The first parameter, whose value should be either
% zero or one, indicates whether the function is expected to signal an
% error when invoked in the manner given.
%
% If the actual result of the call matches the expected result then the test
% case is said to "pass," otherwise it is said to "fail".  It is important
% to understand this: a failed test case is not indicated by an error 
% signal itself, but rather by whether or not the test case expected an
% error to be signaled.
%\notes
%\seealso{tess_catch_type_errors, tess_summary}
%!%- }}}
{
   TestCase++;

   variable args = __pop_args(_NARGS - 2);
   variable func = ();
   variable should_fail = ();	% is error signal expected?
   ERROR_BLOCK
   {
	_clear_error();
	report(func, 1, should_fail);
	return;
   }

   (@func) (__push_args(args));

   report(func, 0, should_fail);
} % }}}

public define tess_load_component(name) % {{{
%!%+{{{
%\function{tess_load_component}
%\synopsis{Evaluate the named S-Lang script, and set the test component name accordingly}
%\usage{tess_load_component(filename)}
%\description
% This function attempts to evalfile() the named script, using the usual
% S-Lang load mechansism, and will set the TESS test component name to 
% the filename if found.
%
% The test component name is printed in the heading of results summaries,
% and uniquely identifies a given test script.  Typically the test component
% name is set to the "basename" of the test script itself (e.g. a script
% add.t sets Component = "add").  This function provides a means of 
% customizing that default behavior while loading additional functionality
% to be exercised within the test script.
%\notes
%\seealso{}
%!%- }}}
{
   !if (string_match(name,".sl$",1))
       name = name + ".sl";

   () = evalfile(name);

   Component = name;
} % }}}

public define tess_summary() % {{{
%!%+{{{
%\function{tess_summary}
%\synopsis{Summarize the results of a suite of tests}
%\usage{Integer_Type tess_summary()}
%\description
% TESS automatically records the pass/fail result of each test case
% executed by tess_invoke.  By default the results of this tally
% are emitted to stdout when tess_summary is called, although this may
% be disabled by calling tess_auto_summarize(0).  The return value
% indicates the number of failed tests.
%\notes
% Under normal circumstances it should not be necessary to call this
% function explicitly, since TESS transparently installs an exit handler
% which calls tess_summary at application termination.  Its return value
% is then passed to the operating so that, for example, a non-zero status
% may be used to fatally terminate a "make test" goal.
% 
%\seealso{tess_invoke}
%!%- }}}
{
   if (_tess_auto_summarize) {
	() = printf("\n=============== %s Test Summary ===============\n",
		  						Component);
	() = printf("\n\t\tNumber of Failures: %d",Results[0]);
	() = printf("\n\t\tNumber of Passes  : %d\n\n",Results[1]);
	() = fflush(stdout);
   }

   Results[0];	% return number of failures
} % }}}

% Local customizations {{{
tess_add_import_paths(Src);
tess_add_eval_paths(".", Src, Share, Packages);
tess_catch_type_errors(1);

private variable Package_Common = path_concat(".","tess-common.sl");
if (stat_file(Package_Common) != NULL)
   () = evalfile(Package_Common);

if (is_defined("atexit"))
	eval("atexit(&tess_summary);");
% }}}

#if$TESS_VERBOSE
() = printf("Loaded TESS, version %s\n",_tess_version_string);
#endif

% Help file installation {{{
$1 = path_concat (path_concat (path_dirname (__FILE__), "help"),"tess.hlp");
if (NULL != stat_file ($1)) {
#ifexists add_help_file
add_help_file($1);
#endif
#ifexists add_doc_file
add_doc_file ($1);
#endif
}
% }}}

provide("tess");
